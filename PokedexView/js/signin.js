window.onload = init;

function init() {
    document.querySelector('.btn-secondary').addEventListener('click', function() {
        window.location.href = "login.html";
    });
    document.querySelector('.btn-primary').addEventListener('click', signin);
}

function signin(){
    var mail = document.getElementById("input-mail").value;
    var name = document.getElementById("input-name").value;
    var pass = document.getElementById("input-password").value;

    axios({
        method: 'post',
        url: 'http://localhost:3000/user/signin',
        data: {
            name: name,
            mail: mail,
            pass: pass
        }      
    }).then(res => {
        if (res.data.code == 0) {
            console.log(res.data.message);
            window.location.href = 'pokemon.html';
        }
        else if (res.data.code == 1){
            alert('Usuario y/o contraseña incorrectos');
        }
    }).catch(err => {
        console.log(err);
    });
}
