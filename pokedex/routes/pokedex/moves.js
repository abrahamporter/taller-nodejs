const express = require('express');
const db = require('../../config/database');

const moves = express();

// Home
moves.get("/", (req, res) => {
    queryAllMoves(req, res);
});

// ----------- GET -----------
// GET random Move
moves.get("/random", (req, res) => {
    const randomNumber = createRandomNumber(2, 621);
    console.log(randomNumber)
    queryMoveById(randomNumber, req, res);
});
// GET Move by id
moves.get("/:id([0-9]{1,3})", (req, res, next) => {
    const id = req.params.id;
    (isNaN(id)) && next();
    queryMoveById(id, req, res);
});
// GET Move by name
moves.get("/:name", (req, res) => {
    const name = req.params.name.toLowerCase();
    queryMoveByName(name, req, res);
});

// ----------- POST -----------
// POST a new Move
moves.post('/', (req, res) => {
    insertNewMove(req, res);
});

// ----------- DELETE -----------
// DELETE a Move by id
moves.delete('/:id([0-9]{1,3})', (req, res) => {
    const id = req.params.id;
    deleteMove(id, res);
});

// ----------- PUT -----------
// UPDATE a Move by id
moves.put('/:id([0-9]{1,3})', (req, res) => {
    const id = req.params.id;
    const columns = Object.keys(req.body)
    const values = Object.values(req.body)
    updateMove(id, columns, values, res);
});


// ----------- QUERYS -----------
// QUERY SELECT all Move
const queryAllMoves = (req, res) => {
    db.query('SELECT * FROM moves').then(rows => {
        res.status(200);
        res.json(rows);
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió un error.')
    });
}
// QUERY SELECT a Move by id form the database
const queryMoveById = (id, req, res) => {
    db.query(`SELECT * FROM moves WHERE move_id = '${id}'`).then(rows => {
        (rows.length > 0)
            ? res.status(200).json(rows)
            : res.status(404).send('El movimiento no se encuentra en la base de datos');
    }).catch(err =>{
        console.log(err);
        res.status(500).send('Ocurrió un error.')
    });
}
// QUERY SELECT a Move by name form the database
const queryMoveByName = (name, req, res) => {
    db.query(`SELECT * FROM moves WHERE move_name = '${name}'`).then(rows => {
        (rows.length > 0)
            ? res.status(200).json(rows)
            : res.status(404).send('El movimiento no se encuentra en la base de datos');
    }).catch(err =>{
        console.log(err);
        res.status(500).send('Ocurrió un error.');
    });
}
// QUERY INSERT a new Move
const insertNewMove = (req, res) => {
    query = `INSERT INTO moves (move_name, type_id, move_power, move_pp, move_accuracy)`;
    query += `VALUES ('${req.body.move_name}', '${req.body.type_id}', '${req.body.move_power}', '${req.body.move_pp}', '${req.body.move_accuracy}')`;
    db.query(query).then(rows => {
        (rows.affectedRows > 0) && res.status(201).send('Movimiento añadido con éxito!')
    }).catch(err => {
        console.log(err);
        res.status(500).send('Ocurrió un error.')
    })
}
// QUERY DELETE Move by id
const deleteMove = (id, res) => {
    query = `DELETE FROM moves WHERE move_id=${id}`;
    db.query(query).then(rows => {
        res.status(200);
        console.log(rows);
        res.send('Movimiento eliminado correctamente');
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió algo mal');
    })


}
// QUERY UPDATE Move by id
const updateMove = (id, columns, values, res) => {
    query = 'UPDATE moves SET ';
    for(let i = 0; i < columns.length; i++){
        query += `${columns[i]} = `;
        query += isNaN(values[i]) ? `'${values[i]}'` : `${values[i]}`;
        (i + 1 < columns.length) ? query += ', ' : query += ' '; 
    }
    query += `WHERE move_id = ${id}`;
    // res.send(query);
    db.query(query).then(rows => {
        res.status(200);
        console.log(rows);
        res.send('Movimiento actualizado correctamente');
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió algo mal');
    })
}

//  Create a random number
const createRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

module.exports = moves;