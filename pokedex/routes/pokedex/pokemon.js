const express = require('express');
const db = require('../../config/database');

const pokemon = express();

// Home
pokemon.get("/", (req, res) => {
    queryAllPokemon(req, res);
});

// ----------- GET -----------
// GET random Pokemon
pokemon.get("/random", (req, res) => {
    const randomNumber = createRandomNumber(1, 722);
    queryPokemonById(randomNumber, req, res);
});
// GET Pokemon by id
pokemon.get("/:id", (req, res, next) => {
    const id = req.params.id;
    (isNaN(id)) && next();
    queryPokemonById(id, req, res);
});
// GET Pokemon by name
pokemon.get("/:name", (req, res) => {
    const name = req.params.name.toLowerCase();
    queryPokemonByName(name, req, res);
});

// ----------- POST -----------
// POST a new Pokemon
pokemon.post('/', (req, res) => {
    insertNewPokemon(req, res);
});

// ----------- DELETE -----------
// DELETE a Pokemon by id
pokemon.delete('/:id', (req, res) => {
    const id = req.params.id;
    deletePokemon(id, res);
});

// ----------- PUT -----------
// UPDATE a Pokemon by id
pokemon.put('/:id([0-9]{1,3})', (req, res) => {
    const id = req.params.id;
    const columns = Object.keys(req.body)
    const values = Object.values(req.body)
    updatePokemon(id, columns, values, res);
});


// ----------- QUERYS -----------
// QUERY SELECT all Pokemon
const queryAllPokemon = (req, res) => {
    db.query('SELECT * FROM pokemon').then(rows => {
        res.status(200);
        res.json(rows);
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió un error.')
    });
}
// QUERY SELECT a Pokemon by id form the database
const queryPokemonById = (id, req, res) => {
    db.query(`SELECT * FROM pokemon WHERE pok_id = '${id}'`).then(rows => {
        res.status(200);
        res.json(rows);
    }).catch(err =>{
        console.log(err);
        res.status(500).send('Ocurrió un error.')
    });
}
// QUERY SELECT a Pokemon by name form the database
const queryPokemonByName = (name, req, res) => {
    db.query(`SELECT * FROM pokemon WHERE pok_name = '${name}'`).then(rows => {
        (rows.length > 0)
            ? res.status(200).json(rows)
            : res.status(404).send('El pokemon no se encuentra en la base de datos');
    }).catch(err =>{
        console.log(err);
        res.status(500).send('Ocurrió un error.');
    });
}
// QUERY INSERT a new Pokemon
const insertNewPokemon = (req, res) => {
    query = `INSERT INTO pokemon (pok_name, pok_height, pok_weight, pok_base_experience)`;
    query += `VALUES ('${req.body.pok_name}', '${req.body.pok_height}', '${req.body.pok_weight}', '${req.body.pok_base_experience}')`;
    db.query(query).then(rows => {
        (rows.affectedRows > 0) && res.status(201).send('Pokemon añadido con éxito!')
    }).catch(err => {
        console.log(err);
        res.status(500).send('Ocurrió un error.')
    })
}
// QUERY DELETE Pokemon by id
const deletePokemon = (id, res) => {
    query = `DELETE FROM pokemon WHERE pok_id=${id}`;
    db.query(query).then(rows => {
        res.status(200);
        console.log(rows);
        res.send('Pokemon eliminado correctamente');
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió algo mal');
    })


}
// QUERY UPDATE Pokemon by id
const updatePokemon = (id, columns, values, res) => {
    query = 'UPDATE pokemon SET ';
    for(let i = 0; i < columns.length; i++){
        query += `${columns[i]} = `;
        query += isNaN(values[i]) ? `'${values[i]}'` : `${values[i]}`;
        (i + 1 < columns.length) ? query += ', ' : query += ' '; 
    }
    query += `WHERE pok_id = ${id}`;
    // res.send(query);
    db.query(query).then(rows => {
        res.status(200);
        console.log(rows);
        res.send('Pokemon actualizado correctamente');
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió algo mal');
    })
}

//  Create a random number
const createRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

module.exports = pokemon;