const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const user = require('./routes/user')
const cors = require('./middleware/corsHandler');
const notFoundHandler = require('./middleware/notFoundHandler');
const auth = require('./middleware/authHandler')

const app = express();

app.use(cors);
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(auth)
app.use('/user', user)
app.use(notFoundHandler);

// Run server
// @param port
// @param function to execute
app.listen(3000, () => {
    console.log("Server is running in port 3000...");
});