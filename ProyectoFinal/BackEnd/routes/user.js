const express = require('express');
const db = require('../config/database');
const jwt = require('jsonwebtoken');

const user = express();

user.post("/signin", (req, res) =>{
    const mail = req.body.mail;
    const name = req.body.name;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const address = req.body.address;
    const pass = req.body.pass;

    query = "INSERT INTO user (user_mail, user_name, user_last_name, user_phone, user_address, user_password) ";
    query += `VALUES ( '${mail}', '${name}', '${lastName}', '${phone}', '${address}', '${pass}');`

    db.query(query).then(rows => {
        (rows.affectedRows > 0) && res.status(201).send('Usuario registrado con éxito!')
    }).catch(err => {
        console.log(err);
        res.status(500).send('Ocurrió un error.')
    });
});

user.post("/login", (req, res) =>{
    const mail = req.body.mail;
    const pass = req.body.pass;
    query = 'SELECT * FROM user WHERE ';
    query += `user_mail ='${mail}' AND user_password = '${pass}';`

    db.query(query).then(rows => {
        res.status(200);
        if(rows.length == 1){
            const token = jwt.sign({
                    id: rows[0].user_id,
                    mail: rows[0].user_mail
                }, 'debugkey');
            res.status(200)
                .json({code: 0, message: token, admin: rows[0].user_admin, user_id: rows[0].user_id});
        }else{
            res.status(200)
                .json({code: 1, message: 'Usuario no encontrado'});
        }
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.send('Algo salió mal');
    });
});

user.get("/all", (req, res) => {
    query = 'SELECT user_id, user_mail, user_name, user_last_name, user_phone, user_address, user_admin FROM user;';

    db.query(query).then(rows => {
        res.status(200);
        res.json(rows);
    }).catch(err => {
        console.log(err);
        res.status(500);
        res.send('Algo salió mal');
    });
});

user.get("/:id([0-9]{1,3})", (req, res, next)  => {
    const id = req.params.id;
    (isNaN(id)) && next();
    query = 'SELECT user_id, user_mail, user_name, user_last_name, user_phone, user_address, user_admin FROM user ';
    query += `WHERE user_id = ${id};`

    console.log(query)
    db.query(query).then(rows => {
        (rows.length > 0)
            ? res.status(200).json(rows)
            : res.status(404).send('El usuario no se encuentra en la base de datos.');
    }).catch(err =>{
        console.log(err);
        res.status(500).send('Ocurrió un error.');
    });
});

user.get("/:name", (req, res)  => {
    const name = req.params.name;
    query = 'SELECT user_id, user_mail, user_name, user_last_name, user_phone, user_address, user_admin FROM user ';
    query += `WHERE user_name LIKE '%${name}%';`

    console.log(query)
    db.query(query).then(rows => {
        (rows.length > 0)
            ? res.status(200).json(rows)
            : res.status(404).send('El usuario no se encuentra en la base de datos.');
    }).catch(err =>{
        console.log(err);
        res.status(500).send('Ocurrió un error.');
    });
});

user.put("/:id", (req, res)  => {
    const id = req.params.id;
    const mail = req.body.mail;
    const name = req.body.name;
    const lastName = req.body.lastName;
    const phone = req.body.phone;
    const address = req.body.address;

    query = `UPDATE user SET user_mail='${mail}', user_name='${name}', user_last_name='${lastName}', user_phone='${phone}', user_address='${address}'`
    query += `WHERE user_id=${id}`;

    // res.send(query);
    db.query(query).then(rows => {
        res.status(200);
        console.log(rows);
        res.send('Usuario actualizado correctamente');
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió algo mal');
    });
});

user.delete('/:id', (req, res) => {
    const id = req.params.id;

    query = `DELETE FROM user WHERE user_id=${id}`;
    db.query(query).then(rows => {
        res.status(200);
        console.log(rows);
        res.send('Usuario eliminado correctamente');
    }).catch(err =>{
        console.log(err);
        res.status(500);
        res.send('Ocurrió algo mal');
    })
});
module.exports = user;