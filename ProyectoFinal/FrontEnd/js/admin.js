window.onload = init();
let users = []
let activeEditId = 0;
let activeDeleteId = 0;

function init() {
    if(localStorage.getItem("token")){
        if(localStorage.getItem("admin") == 1){

        }else{
            window.location.href = "user.html";
        }
    } else{
        window.location.href = "index.html";
    }
    loadUsers();
}

function loadUsers() {
    axios.get("http://localhost:3000/user/all")
        .then(res => {
            users = res.data
            displayUsers(users);
        })
        .catch(err => {
            console.log(err);
        });
}

function displayUsers(data) {
    document.getElementById('user-list').innerHTML = '';
    data.forEach(user => {
        document.getElementById('user-list').innerHTML +=
        `
        <div id="user" class=" col-md-10">
            <div id="${user.user_id}-main" class="user-main row col-md-12 pt-1 pb-1 mt-1 mb-1">
                <div class="col-10 text-left align-self-center" onclick="displayUserDetails(${user.user_id})" style="cursor: pointer;">
                    ${user.user_name} ${user.user_last_name}
                </div>
                <div class="col-1 align-self-center" onclick="editUser(${user.user_id})" data-toggle="modal" data-target="#edit-modal">
                    <span id="edit-user-btn" class="text-success" style="cursor: pointer;">
                        <i class="fa fa-pencil"></i>
                    </span>
                </div>
                <div class="col-1 align-self-center" onclick="deleteUser(${user.user_id})" data-toggle="modal" data-target="#delete-modal">
                    <span id="delete-user-btn" class="text-danger" style="cursor: pointer;">
                        <i class="fa fa-times"></i>
                    </span>
                </div>
            </div>
            <div id="${user.user_id}" class="user-details row col-md-12" style="display: none">
                <p class="col-12 text-left pl-4">Email: ${user.user_mail}</p>
                <p class="col-12 text-left pl-4">Teléfono: ${user.user_phone}</p>
                <p class="col-12 text-left pl-4">Dirección: ${user.user_address}</p>
            </div>
        </div>
        `
    });
}

function displayUserDetails(id){
    let userMain = document.getElementById(id+'-main');
    let userDetails = document.getElementById(id);

    if(userDetails.style.display === 'none'){
        userMain.style.background = '#97adad';
        userDetails.style.display = 'block';
        var allMain = document.getElementsByClassName('user-main');
        var allDetails = document.getElementsByClassName('user-details');
        for (var i = 0; i < allMain.length; i++) {
            allMain[i].style.background = 'none';
            allDetails[i].style.display = 'none';
        }
        userMain.style.background = '#97adad';
        userDetails.style.display = 'block';
    }else{
        userMain.style.background = 'none';
        userDetails.style.display = 'none';
    }
}
function addUser(){
    const name = document.getElementById('add-name').value;
    const lastName = document.getElementById('add-last-name').value;
    const password = document.getElementById('add-password').value;
    const mail = document.getElementById('add-mail').value;
    const phone = document.getElementById('add-phone').value;
    const address = document.getElementById('add-address').value;
    axios({
        method: 'post',
        url: 'http://localhost:3000/user/signin',
        data: {
            name: name,
            lastName: lastName,
            mail: mail,
            phone: phone,
            address: address
        }      
    }).then(res => {
        console.log(res.data);
        $('#add-modal').modal('hide');
        $('#add-success').toast('show');
        loadUsers();
    }).catch(err => {
        console.log(err);
    });
    loadUsers();
}

function editUser(userId){
    activeEditId = userId;
    user = users.filter(user => user.user_id == userId)[0];
    const nameInput = document.getElementById('edit-name');
    const lastNameInput = document.getElementById('edit-last-name');
    const mailInput = document.getElementById('edit-mail');
    const phoneInput = document.getElementById('edit-phone');
    const addressInput = document.getElementById('edit-address');

    nameInput.value = user.user_name;
    lastNameInput.value = user.user_last_name;
    mailInput.value = user.user_mail;
    phoneInput.value = user.user_phone;
    addressInput.value = user.user_address;
}
function saveEdit(){
    const name = document.getElementById('edit-name').value;
    const lastName = document.getElementById('edit-last-name').value;
    const mail = document.getElementById('edit-mail').value;
    const phone = document.getElementById('edit-phone').value;
    const address = document.getElementById('edit-address').value;
    axios({
        method: 'put',
        url: 'http://localhost:3000/user/' + activeEditId,
        data: {
            name: name,
            lastName: lastName,
            mail: mail,
            phone: phone,
            address: address
        }      
    }).then(res => {
        console.log(res.data);
        $('#edit-modal').modal('hide');
        $('#edit-success').toast('show');
    }).catch(err => {
        console.log(err);
    });
    loadUsers();
}

function deleteUser(id) {
    activeDeleteId = id;
}
function deleteConfirmed() {
    axios({
        method: 'delete',
        url: 'http://localhost:3000/user/' + activeDeleteId,
    }).then(res => {
        console.log(res.data);
        $('#delete-modal').modal('hide');
        $('#delete-success').toast('show');
    }).catch(err => {
        console.log(err);
    });
    document.getElementById('user-list').innerHTML = '';
    loadUsers();
}

function search(){
    searchValue = document.getElementById('search-input').value;
    axios.get("http://localhost:3000/user/" + searchValue)
        .then(res => {
            displayUsers(res.data);
        })
        .catch(err => {
            console.log(err);
        });
}

function logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("admin");
    localStorage.removeItem("user_id");
    window.location.href = "index.html";
}