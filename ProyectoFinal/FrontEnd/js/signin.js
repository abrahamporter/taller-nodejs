window.onload = init;

function init() {
    document.getElementById('log-in').addEventListener('click', function() {
        window.location.href = "login.html";
    });
    document.getElementById('sign-in').addEventListener('click', signin);
}

function signin(){
    var mail = document.getElementById("input-mail").value;
    var password = document.getElementById("input-password").value;
    var name = document.getElementById("input-name").value;
    var lastName = document.getElementById("input-last-name").value;
    var phone = document.getElementById("input-phone").value;
    var address = document.getElementById("input-address").value;

    axios({
        method: 'post',
        url: 'http://localhost:3000/user/signin',
        data: {
            mail: mail,
            password: password,
            name: name,
            lastName: lastName,
            phone: phone,
            address: address
        }      
    }).then(res => {
        if (res.data.code == 0) {
            console.log(res.data);
            localStorage.setItem('token', res.data.message);
            window.location.href = 'admin.html';
        }
        else if (res.data.code == 1){
            alert('Usuario y/o contraseña incorrectos');
        }
    }).catch(err => {
        console.log(err);
    });
}
