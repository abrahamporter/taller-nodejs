window.onload = init();
let users = []
let activeEditId = 0;
let activeDeleteId = 0;

function init() {
    if(localStorage.getItem("token")){
        if(localStorage.getItem("admin") == 1){
            window.location.href = "admin.html";
        }
    } else{
        window.location.href = "index.html";
    }
    loadUserInfo();
}

function loadUserInfo() {
    const id = localStorage.getItem("user_id")
    axios.get("http://localhost:3000/user/" + id)
        .then(res => {
            displayUserInfo(res.data[0]);
        })
        .catch(err => {
            console.log(err);
        });
}

function displayUserInfo(info) {
    console.log(info)
    const mailInfo = document.getElementById('mail-info');
    const nameInfo = document.getElementById('name-info');
    const lastNameInfo = document.getElementById('last-name-info');
    const phoneInfo = document.getElementById('phone-info');
    const addressInfo = document.getElementById('address-info');
    mailInfo.innerHTML = info.user_mail;
    nameInfo.innerHTML = info.user_name;
    lastNameInfo.innerHTML = info.user_last_name;
    phoneInfo.innerHTML = info.user_phone;
    addressInfo.innerHTML = info.user_address;
}

function editUser(userId){
    activeEditId = userId;
    user = users.filter(user => user.user_id == userId)[0];
    const nameInput = document.getElementById('edit-name');
    const lastNameInput = document.getElementById('edit-last-name');
    const mailInput = document.getElementById('edit-mail');
    const phoneInput = document.getElementById('edit-phone');
    const addressInput = document.getElementById('edit-address');

    nameInput.value = user.user_name;
    lastNameInput.value = user.user_last_name;
    mailInput.value = user.user_mail;
    phoneInput.value = user.user_phone;
    addressInput.value = user.user_address;
}
function saveEdit(){
    const name = document.getElementById('edit-name').value;
    const lastName = document.getElementById('edit-last-name').value;
    const mail = document.getElementById('edit-mail').value;
    const phone = document.getElementById('edit-phone').value;
    const address = document.getElementById('edit-address').value;
    axios({
        method: 'put',
        url: 'http://localhost:3000/user/' + activeEditId,
        data: {
            name: name,
            lastName: lastName,
            mail: mail,
            phone: phone,
            address: address
        }      
    }).then(res => {
        console.log(res.data);
        $('#edit-modal').modal('hide');
        $('#edit-success').toast('show');
    }).catch(err => {
        console.log(err);
    });
    loadUsers();
}

function logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("admin");
    localStorage.removeItem("user_id");
    window.location.href = "index.html";
}