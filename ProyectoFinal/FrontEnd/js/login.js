window.onload = init;

function init() {
    document.getElementById('sign-in').addEventListener('click', function() {
        window.location.href = "signin.html";
    });
    document.getElementById('log-in').addEventListener('click', login);
}

function login(){
    var mail = document.getElementById("input-mail").value;
    var pass = document.getElementById("input-password").value;

    axios({
        method: 'post',
        url: 'http://localhost:3000/user/login',
        data: {
            mail: mail,
            pass: pass
        }      
    }).then(res => {
        if (res.data.code == 0) {
            console.log(res.data);
            localStorage.setItem('token', res.data.message);
            localStorage.setItem('admin', res.data.admin);
            localStorage.setItem('user_id', res.data.user_id);
            window.location.href = 'admin.html';
        }
        else if (res.data.code == 1){
            alert('Usuario y/o contraseña incorrectos');
        }
    }).catch(err => {
        console.log(err);
    });
}
