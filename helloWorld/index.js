const express = require('express');
const app = express();

var persons = {};

// Handle requests to https://127.0.0.1:300/
app.get("/", (req, res) => {
    res.send("Hola mundo");
})

// app.get("/:name/:lastName/:age/:city", (req, res) => {
//     const data = req.params;
//     console.log(data)
//     res.send(data);
// })

app.get("/:name/:age", (req, res) => {
    const name = req.params.name;
    const age = req.params.age;

    persons[name] = age;
    console.log(persons);
    res.send(persons);
})

// Run server
// @params: puerto, función a ejecutar el servidor
app.listen(3000, () => {
    console.log("Server is running...");
});